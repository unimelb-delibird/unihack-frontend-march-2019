import React, {Component} from 'react';
import Market from './Components/market/Market.jsx'
import './App.css';
import Welcome from "./Components/welcome/welcome.jsx";
import PrimarySearchAppBar from './Components/appBar'
import NewProblem from "./Components/newProblem/newProblem";
import SolutionUpload from "./Components/upload/uploadPage";

import firebase from "firebase";

// Configure Firebase.
const config = {
  apiKey: "AIzaSyBcbSSjge10MSlvqvP2GIqjEmzfHiOihsY",
  authDomain: "unihack2019-234701.firebaseapp.com",
  databaseURL: "https://unihack2019-234701.firebaseio.com",
  projectId: "unihack2019-234701",
  storageBucket: "unihack2019-234701.appspot.com",
  messagingSenderId: "908032794248"
};
firebase.initializeApp(config);

const db = firebase.firestore()

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panel:"welcome"
    };
  }

  switchPanel = (new_panel_name) => {
    this.setState({panel:new_panel_name})
  }

  render() {
    // react bootstrap by default use 12 columns
    return (
      <div className="App" >
          <PrimarySearchAppBar panelSwitcher={this.switchPanel} firebase={firebase}/>
          {{
            "market":<Market panelSwitcher={this.switchPanel} showPage={-1} firestore={db} firebase={firebase}/>,
            "welcome":<Welcome panelSwitcher={this.switchPanel} style={{height:"200px"}}/>,
            "upload":<SolutionUpload/>,
            "newProblem":<NewProblem panelSwitcher={this.switchPanel} firestore={db}/>
          }[this.state.panel]}
      </div>
    );
  }
}

export default App;


