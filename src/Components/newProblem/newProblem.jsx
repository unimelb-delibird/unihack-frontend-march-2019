import React from 'react'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const styles = {
  paperContainer: {
    height: "95%",
    width: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  intro_title: {
    fontSize: "5rem",
    textAlign: "center"
  },
  intro_des: {
    color: "white",
    fontSize: "3.6rem",
    textAlign: "center"
  },
  text_area: {
    marginLeft: "25%",
    marginRight: "25%",
    width: "50%",
    fontSize: "200px"
  },
  button: {
    fontSize: "20px",
    marginTop: "40px",
    background: "#2196F3"
  }
};

export default class NewProblem extends React.Component {
  constructor(props) {
    super(props)
    this.firestore = props.firestore
    this.state = {
      problem_title: "",
      problem_description: ""
    }
  }

  handleCreateProblem = () => {
    let problem_title = this.state.problem_title;
    let problem_description = this.state.problem_description;
    this.firestore.collection("problems").add({
      title: problem_title,
      description: problem_description,
      solutions:[]
    }).then(function (docRef) {
      console.log("Document written with ID: ", docRef.id);
    })
      .catch(function (error) {
        console.error("Error adding document: ", error);
      });
    this.props.panelSwitcher("market")
  }

  render() {
    return (
      <div style={{height: window.innerHeight}}>
        <Paper style={styles.paperContainer}>
          <Typography style={styles.intro_title}><br/>Create New Problem</Typography>
          <TextField
            style={styles.text_area}
            id="standard-uncontrolled"
            label="Problem title"
            margin="normal"
            fullWidth={true}
            onChange={(e) => {
              this.setState({problem_title: e.target.value})
            }}
          />
          <Typography><br/></Typography>
          <TextField
            style={styles.text_area}
            id="standard-multiline-static"
            label="Problem description"
            multiline
            rows="4"
            margin="normal"
            fullWidth={true}
            onChange={(e) => {
              this.setState({problem_description: e.target.value})
            }}
          />
          <input
            accept="image/*"
            style={{ display: 'none' }}
            id="raised-button-file"
            multiple
            type="file"
          />
          <label htmlFor="raised-button-file">
            <Button variant="raised" component="span">
              Upload Data Set
            </Button>
          </label>
          <Typography style={styles.intro_title}><br/></Typography>
          <Button variant="contained" color="primary" style={styles.button}
                  onClick={this.handleCreateProblem}>Creat New Problem</Button>
        </Paper>
      </div>
    )
  }
}