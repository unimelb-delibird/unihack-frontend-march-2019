import React from 'react'
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  paperContainer: {
    height: "95%",
    width: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  intro_des: {
    color:"black",
    fontSize:"3.6rem",
    textAlign:"center"
  }
};

export default class Solution extends React.Component {
  constructor(props) {
    super(props)
    this.solution = props.solution
    console.log("received solution", this.solution)
  }

  render() {
    return (
      <div>
        <Typography>Your API URL:<br/> {this.solution.api_url}</Typography>
        <Typography>Your API KEY:<br/> {this.solution.api_key}</Typography>
      </div>
    )
  }
}