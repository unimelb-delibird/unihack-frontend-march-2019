import React from 'react'
import bgImage from './problem_bg.jpg'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SolutionUpload from "./../upload/uploadPage";
import Solution from "./solution";

const styles = {
  paperContainer: {
    backgroundImage: `url(${bgImage})`,
    height: "50%",
    width: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  intro_des: {
    color: "white",
    fontSize: "2.5rem",
    textAlign: "center",
    marginBot: "20px"
  },
  largeSize: {
    fontSize: "20px"
  },
  moreText: {
    marginLeft: "25%",
    marginRight: "25%"
  },
  button: {
    fontSize: "20px",
    marginTop: "40px",
    background: "#2196F3",
    float: "right",
    display: "block",
    marginRight:"10px"
  },
  tableButton: {
    fontSize: "20px",
    background: "grey",
    float: "right"
  }
};

export default class Problem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      solution_open:false,
      solution_showing:0
    };
  }

  handleClickOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleSolutionOpen = (i) => {
    this.setState({solution_open:true, solution_showing:i})
  }

  handleSolutionClose = () => {
    this.setState({solution_open:false})
  }

  render() {
    return (
      <div style={{width: "100%"}}>
        <Paper style={styles.paperContainer}>
          <Typography style={styles.intro_des}><br/><b>{this.props.problem.title}</b></Typography>
          <Typography style={styles.intro_des}><br/></Typography>
        </Paper>
        <Paper>
          <Typography align="left" variant="h4" style={styles.moreText}><br/>Description</Typography>
          <label htmlFor="raised-button-file" style={{float:"right", marginRight:"10px"}}>
            <Button variant="raised" component="span" >
              Download Data Set
            </Button>
          </label>
          <Typography align="left" variant="h6" style={styles.moreText}>
            <br/>&nbsp;&nbsp;&nbsp;&nbsp;{this.props.problem.description}
          </Typography>
          <Typography><br/></Typography>
        </Paper>
        <Typography><br/></Typography>
        <Typography variant="h4">Solutions</Typography>
        <Button variant="contained" color="primary" style={styles.button}
                onClick={this.handleClickOpen}>New Solution</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          maxWidth="xl"
        >
          <DialogTitle id="form-dialog-title">Solution Upload</DialogTitle>
          <DialogContent>
            <SolutionUpload problem={this.props.problem}
                            firestore={this.props.firestore}
                            firebase={this.props.firebase}/>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell style={styles.largeSize}><b>Solution Title</b></TableCell>
              <TableCell style={styles.largeSize}><b>Score</b></TableCell>
              <TableCell style={styles.largeSize}><b>Price per Call</b></TableCell>
              <TableCell style={styles.largeSize}><b></b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              this.props.problem.solutions.map((solution, i) => {
                return (
                  <TableRow key={i}>
                    <TableCell>{solution.solution_title}</TableCell>
                    <TableCell>{solution.score}</TableCell>
                    <TableCell>{solution.price}</TableCell>
                    <TableCell>
                      <Button variant="contained" color="primary" style={styles.tableButton}
                      onClick={this.handleSolutionOpen.bind(this, i)}>Select</Button>
                    </TableCell>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>

        {
          this.state.solution_open &&
          <Dialog
            open={this.state.solution_open}
            onClose={this.handleSolutionClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">{this.props.problem.solutions[this.state.solution_showing].solution_title}</DialogTitle>
            <DialogContent>
              <Solution solution={this.props.problem.solutions[this.state.solution_showing]}/>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleSolutionClose} color="primary">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>

        }
      </div>
    )
  }
}

