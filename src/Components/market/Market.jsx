import React from 'react'
import Problem from "./problem";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import bgImage from './market_bg.jpg'

const styles = {
  paperContainer: {
    backgroundImage: `url(${bgImage})`,
    height: "50%",
    width:"100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  header:{
    color:"black",
    textAlign:"left",
    margin:"10px"
  },
  intro_des:{
    color:"white",
    fontSize:"2.8rem",
    textAlign:"center",
    marginBot:"20px"
  },
  largeSize:{
    fontSize:"20px"
  },
  midSize:{
    fontSize:"15px"
  },
  button:{
    marginTop:"15px",
    marginRight:"5px",
    fontSize: "20px",
    background: "#2196F3",
    float:"right"
  },
  hyperL:{
    cursor:"pointer",
    color:"blue",
    textDecoration:"underline"
  }
}




export default class Market extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      // a list of dictionary
      problems:[
        // {
        //   title:"Image Recognition for count birds in one photo",
        //   description:"count how many bird are there in one photo",
        //   solutions:[{name:"author1", score:"95", price:"0.02"}]
        // },
        // {
        //   title:"Classify Recipts based on shopping type",
        //   description:"descasdasdsa,smdnklasjkdjasd",
        //   solutions:[]
        // },
        // {
        //   title:"Predict stock market based on pass data",
        //   description:"descasdasdsa,smdnklasjkdjasd",
        //   solutions:[]
        // }
        ],
      showPage:-1
    }
    this.firestore=props.firestore
    this.getAllProblems()
  }

  getAllProblems = () => {
    console.log("getAll Problem")
    let problems = this.state.problems
    this.firestore.collection("problems").get().then((querySnapshot)=> {
      querySnapshot.forEach(function(doc) {
        // doc.data() is never undefined for query doc snapshots
        let data = doc.data()
        let problem_id = doc.id
        problems.push({
          id:problem_id,
          title:data.title,
          description:data.description,
          solutions:data.solutions
        });
      });
      this.setState({problems:problems})
    });
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.hasOwnProperty("showPage")) {
      this.setState({showPage:nextProps.showPage})
    }
  }


  showProblem = (i) => {
    this.setState({showPage:i})
  }

  render() {
    console.log("showPage", this.state.showPage)
    return (
      <div>
          {
            this.state.showPage === -1 ?
              <Paper>
                {
                  this.state.problems.length > 0 &&
                  <div>
                  <Paper style={styles.paperContainer}>
                    <Typography><br/></Typography>
                    <Typography style={styles.intro_des}><b>Market Place</b></Typography>
                    <Typography><br/></Typography>
                  </Paper>
                  <Button variant="contained" color="primary" style={styles.button}
                    onClick={() => this.props.panelSwitcher("newProblem")}>New Problem</Button>
                  <Table padding="dense">
                    <TableHead>
                    <TableRow>
                      <TableCell align="left" style={styles.largeSize}><b>Problem Title</b></TableCell>
                      <TableCell align="right" style={styles.largeSize}><b>Num of Solution</b></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {
                      this.state.problems.map((problem, i) => {
                        console.log("problem", problem)
                        return (
                          <TableRow key={i}>
                            <TableCell align="left" onClick={()=>this.showProblem(i)} style={styles.midSize}>
                              <span style={styles.hyperL}>{problem.title}</span>
                            </TableCell>
                            <TableCell align="right" style={styles.midSize}>{problem.solutions.length}</TableCell>
                          </TableRow>
                        )
                      })
                    }
                    </TableBody>
                  </Table>
                  </div>
                }
              </Paper>
              :
              <Problem
                firestore={this.firestore}
                firebase={this.props.firebase}
                problem={this.state.problems[this.state.showPage]}
                panelSwitcher={this.props.panelSwitcher}
              />
          }
      </div>
    )
  }
}