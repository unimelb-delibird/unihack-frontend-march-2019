import React from 'react'
import {Modal} from "react-bootstrap";
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';




export default class LoginPage extends React.Component {
  constructor(props) {
    super(props)

    this.firebase = props.firebase

    this.uiConfig = {
      // Popup signin flow rather than redirect flow.
      signInFlow: 'popup',
      // We will display Google and Facebook as auth providers.
      signInOptions: [
        this.firebase.auth.EmailAuthProvider.PROVIDER_ID,
        this.firebase.auth.GoogleAuthProvider.PROVIDER_ID
      ],
      callbacks: {
        // Avoid redirects after sign-in.
        signInSuccessWithAuthResult: () => {
          this.props.onHide()
        }
      }
    };
  }

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = this.firebase.auth().onAuthStateChanged(
      (user) => {
        console.log("user",user)
        if (user !== null) {
          this.props.signInHandler()
        }
      }
    );
  }

  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }


  render() {
    return (
      <Modal
        {...this.props}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
          <div>
            <div style={{textAlign:"center", width:"100%"}}><b> Sign in or register with one click:</b></div>
            <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={this.firebase.auth()}/>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}