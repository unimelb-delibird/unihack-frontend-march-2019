import React from 'react'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import DropzoneAreaExample from "./DropZone";
import TextField from '@material-ui/core/TextField';

const uuidv4 = require('uuid/v4');

const styles = {
  paperContainer: {
    height: "95%",
    width: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  intro_title: {
    color: "black",
    fontSize: "5rem",
    textAlign: "left"
  },
  button: {
    fontSize: "20px",
    marginTop: "40px",
    background: "#2196F3"
  },
  text_area: {
    marginLeft: "25%",
    marginRight: "25%",
    width: "50%",
    fontSize: "200px"
  }
};

export default class SolutionUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: 0,
      solution_title: uuidv4()
    }
  }

  render() {
    return (
      <div style={{height: window.innerHeight}}>
        <Paper style={styles.paperContainer}>
          <TextField
            style={styles.text_area}
            id="standard-uncontrolled"
            label="Solution Title"
            margin="normal"
            fullWidth={true}
            placeholder={this.state.solution_title}
            onChange={(e) => this.setState({solution_title: e.target.value})}
          />
          <TextField
            style={styles.text_area}
            id="standard-uncontrolled"
            label="Price (Without Hosting Fee)"
            margin="normal"
            fullWidth={true}
            onChange={(e) => this.setState({price: e.target.value})}
          />
          <DropzoneAreaExample problem={this.props.problem}
                               firebase={this.props.firebase}
                               firestore={this.props.firestore}
                               price={this.state.price}
                               solution_title={this.state.solution_title}
          />
        </Paper>
      </div>
    )
  }
}