import React, {Component} from 'react'
import {DropzoneArea} from 'material-ui-dropzone'
import {Button, Typography} from '@material-ui/core'
import axios from 'axios'
const uuidv4 = require('uuid/v4');

class DropzoneAreaExample extends Component{
  constructor(props){
    super(props);
    this.state = {
      files: [],
      loaded:0
    };
  }

  handleChange(files){
    this.setState({
      files: files
    });
  }

  handleSubmit = () => {
    console.log("file submitted", this.state.files)

    let author = this.props.firebase.auth().currentUser.displayName;
    let solution_id = uuidv4().replace(new RegExp('-', 'g'),'_');
    let problem_id = this.props.problem.id

    // determine by the server
    let price = this.props.price;
    let score = Math.random() * 10 + 90;

    let solution={
      solution_title:this.props.solution_title,
      author:author,
      score:score,
      price:price,
      solution_id:solution_id,
      api_key:"cffbf154237f4944b8482e106fa6197a",
      api_url:"https://api.mlm.com/"+solution_id
    }

    let model_filename=problem_id+solution_id

    let solutions = this.props.problem.solutions
    solutions.push(solution)
    console.log("solutions", solutions)

    // write solution into database
    this.props.firestore.collection("problems").doc(problem_id).update({
      solutions:solutions
    })
    .then(function() {
      console.log("Document successfully updated!");
    })
    .catch(function(error) {
      // The document probably doesn't exist.
      console.error("Error updating document: ", error);
    });

    // upload file to cloud storage
    let storageRef = this.props.firebase.storage().ref()
    let mountainsRef = storageRef.child("Models");
    let mountainImagesRef = storageRef.child(model_filename);
    mountainImagesRef.put(this.state.files[0]).then((snapshot)=>{
      console.log("Uploaded a file", model_filename, snapshot)
    })


  }

  render(){
    return (
      <div>
        <DropzoneArea
          dropzoneText={'Please upload a zip file that contains all model files without any folder'}
          acceptedFiles={['.zip', 'application/x-zip-compressed']}
          filesLimit={1}
          showPreviewsInDropzone={false}
          showPreviews={true}
          showFileNamesInPreview={true}
          maxFileSize={100000000}
          onChange={this.handleChange.bind(this)}
        />
        <Button color="inherit" onClick={this.handleSubmit} disabled={this.state.files.length === 0}>submit</Button>
      </div>
    )
  }
}

export default DropzoneAreaExample;