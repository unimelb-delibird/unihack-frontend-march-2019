import React from 'react'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import bgImage from './market.jpg'


const styles = {
  paperContainer: {
      backgroundImage: `url(${bgImage})`,
      height: "95%",
      width:"100%",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover"
  },
  intro_title:{
    color:"white",
    fontSize:"5rem",
    textAlign:"center"
  },
  intro_des:{
    color:"white",
    fontSize:"3.6rem",
    textAlign:"center"
  },
  button:{
    fontSize: "20px",
    marginTop:"40px",
    background: "#2196F3"
  }
};

export default class Welcome extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div style={{height:window.innerHeight}}>
        <Paper style={styles.paperContainer}>
          <Typography style={styles.intro_title}><br/><br/>Welcome to MLM</Typography>
          <Typography style={styles.intro_des}>Open Machine Learning Market for Everyone</Typography>
          <Button variant="contained" color="primary" style={styles.button}
            onClick={() => this.props.panelSwitcher("market")}>Explore The Marketplace!</Button>
        </Paper>
        
      </div>
    )
  }
}